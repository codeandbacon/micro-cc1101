from machine import Pin, SPI
import time
from definitions import *


spi = SPI(1, baudrate=200000, polarity=1, phase=0)
spi.init()

cs = Pin(15, Pin.OUT)

gd0 = Pin(4, Pin.IN)
gd2 = Pin(5, Pin.IN)

class CC1101(object):

    def __init__(self, spi_obj, chip_select_pin, gd0_pin, gd2_pin=None):
        self.spi = spi_obj
        self.cs = chip_select_pin
        self.gd0 = gd0_pin
        while gd0.value():
            time.sleep(1)
            print('Chip not ready, waiting...')

    def _byte_to_bits(self, byte):
        byte_string = '{0:b}'.format(byte)
        complete_string = ''
        return ('0' * (8 - len(byte_string))) + byte_string

    def _status(self, status_byte):
        status = self._byte_to_bits(status_byte)
        rdy = status[0]
        state = status[1:3]
        fifo = status[4:7]
        print('Status byte: ready [{}], state [{}], fifo [{}]'.format(rdy, state, fifo))

    def write(self, addr, data):
        self.cs.value(0)
        write_buf = bytearray([addr, data])
        read_buf = bytearray(2)
        self.spi.write_readinto(write_buf, read_buf)
        print(read_buf)
        self.cs.value(1)

    def _decode_resp(self, resp, values):
        if values:
            for k, v in values.items():
                if type(k) == tuple:
                    start = 7 - k[0]
                    end = 8 - k[1]
                    r = int(resp[start:end], 2)
                    if v:
                        if type(v) == dict:
                            curr = v.get(r)
                        else:
                            curr = v
                        print('[{}:{}]: {} -> {}'.format(k[0], k[1], r, curr))
                else:
                    r = resp[7-k]
                    print('[{}]: {} -> {}'.format(k, r, v))
        else:
            pass

    def write_single(self, addr, data):
        self.write(addr.get('addr') + WRITE_ONE, data)

    def read(self, addr):
        # set cs low before sending data
        self.cs.value(0)
        
        # keep only the second byte
        status, resp = self.spi.read(2, addr)

        # set high before return
        self.cs.value(1)
        return status, resp

    def read_single(self, code):
        print('=============================')
        addr = code.get('addr')
        if addr >= 0x30:
            raise Exception('Status register cannot be accessed in single byte mode')
        status, resp = self.read(addr + READ_ONE)
        print('Reading: {} ({})'.format(code.get('desc'), hex(addr)))
        print('Response: {} ({})'.format(self._byte_to_bits(resp), hex(resp)))
        self._decode_resp(self._byte_to_bits(resp), code.get('resp'))
        self._status(status)
        print('=============================\n')
        

    def read_burst(self, code):
        print('=============================')
        addr = code.get('addr')
        status, resp = self.read(addr + READ_BURST)
        print('Reading: {} ({})'.format(code.get('desc'), hex(addr)))
        print('Response: {} ({})'.format(self._byte_to_bits(resp), hex(resp)))
        self._status(status)
        self._decode_resp(self._byte_to_bits(resp), code.get('resp'))
        print('=============================\n')

cc = CC1101(spi, cs, gd0)
# cc.read_burst(PARTNUM)
# cc.read_burst(VERSION)
cc.read_single(FIFOTHR)
cc.read_single(SYNC1)
# cc.read_burst(MARCSTATE)
cc.read_single(IOCFG0)
cc.read_single(IOCFG2)
# cc.write_single(SYNC1, 0xd2)
cc.read_single(SYNC1)
# cc.write_single(SYNC1, 0xd1)
# cc.read_single(SYNC1)
# cc.read_burst(PARTNUM)
# cc.read_burst(VERSION)
# cc.read_single(VERSION)
cc.read_single(PKTCTRL0)
cc.read_single(BSCFG)
