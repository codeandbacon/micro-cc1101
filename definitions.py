### definitions

WRITE_ONE = 0x00
WRITE_BURST = 0x40
READ_ONE = 0x80
READ_BURST = 0xc0

IOCFG2 = {
    'desc': 'GDO2 configuration',
    'addr': 0x00,
    'resp': {
        7: None,
        6: 'invert output',
        (5, 0): {
            0x00: '',
            0x01: '',
            0x02: '',
            0x29: 'CHIP_RDYn',
            0x2e: 'high impedance',
            0x3f: 'CLK_XOSC/192'

        }
    }
}

# disconnected on current board
IOCFG1 = {
    'desc': 'GDO1 configuration',
    'addr': 0x01,
    'resp': {
        7: None,
        6: 'invert output',
        (5, 0): {
            0x00: '',
            0x01: '',
            0x02: '',
            0x29: 'CHIP_RDYn',
            0x2e: 'high impedance',
            0x3f: 'CLK_XOSC/192'

        }
    }
}

IOCFG0 = {
    'desc': 'GDO0 configuration',
    'addr': 0x02,
    'resp': {
        7: 'temperature sensor',
        6: 'invert output',
        (5, 0): {
            0x00: '',
            0x01: '',
            0x02: '',
            0x29: 'CHIP_RDYn',
            0x2e: 'high impedance',
            0x3f: 'CLK_XOSC/192'

        }
    }
}

FIFOTHR = {
    'desc': 'RX/TX treshold',
    'addr': 0x03,
    'resp': {
        7: 'reserved',
        6: 'ADC retention',
        (5, 4): {
            0x00: '0 db',
            0x01: '6 db',
            0x02: '12 db',
            0x03: '18 db'
        },
        (3, 0): {
            0x00: '61 TX / 4 RX',
            0x01: '57 TX / 8 RX',
            0x02: '53 TX / 12 RX',
            0x03: '49 TX / 16 RX',
            0x04: '45 TX / 20 RX',
            0x05: '41 TX / 24 RX',
            0x06: '37 TX / 28 RX',
            0x07: '33 TX / 32 RX',
            0x08: '29 TX / 36 RX',
            0x09: '25 TX / 40 RX',
            0x0a: '21 TX / 44 RX',
            0x0b: '17 TX / 48 RX',
            0x0c: '13 TX / 52 RX',
            0x0d: '9 TX / 56 RX',
            0x0e: '5 TX / 60 RX',
            0x0f: '1 TX / 64 RX',
        }
    }
}

SYNC1 = {
    'desc': 'sync word',
    'addr': 0x04,
    'resp': {
        (7, 0): 'sync word'
    }
}

SYNC0 = {
    'desc': 'sync word',
    'addr': 0x05,
    'resp': {
        (7, 0): 'sync word'
    }
}

PKTLEN = {
    'desc': 'packet length',
    'addr': 0x06,
    'resp': {
        (7, 0): 'packet length'
    }
}

PKTCTRL1 = {
    'desc': '',
    'addr': 0x07,
    'resp': {
        (7, 5): {},
        4: '',
        3: '',
        2: '',
        (1, 0): {
            0x00: '',
            0x01: '',
            0x02: '',
            0x03: '',
        }
    }
}

PKTCTRL0 = {
    'desc': 'Packet automation control',
    'addr': 0x08,
    'resp': {
        7: None,
        6: 'WHITE_DATA',
        (5, 4): {
            0x00: 'normal mode',
            0x01: 'synchronous',
            0x02: 'random',
            0x03: 'asynchronous'
        },
        3: None,
        2: 'CRC_EN',
        (1, 0): {
            0x00: 'fixed length',
            0x01: 'variable length',
            0x02: 'infinite length',
            0x03: 'reserved',
        }
    }
}

ADDR = {
    'desc': 'Device address',
    'addr': 0x09,
    'resp': {
        (7, 0): None
    }
}

CHANNR = {
    'desc': '',
    'addr': 0x0a,
    'resp': {
        (7, 0): None
    }
}

FSCTRL1 = {
    'desc': 'Frequency Synthesizer',
    'addr': 0x0b,
    'resp': {
        (7, 6): None,
        5: None,
        (4, 0): None
    }
}

FSCTRL0 = {
    'desc': 'Frequency Synthesizer',
    'addr': 0x0c,
    'resp': {
        (7, 0): None
    }
}


FREQ2 = {
    'desc': None,
    'addr': 0x0d,
    'resp': {
        (7, 6): None,
        (5, 0): None
    }
}

FREQ1 = {
    'desc': None,
    'addr': 0x0e,
    'resp': {
        (7, 6): None,
        (5, 0): None
    }
}

FREQ0 = {
    'desc': None,
    'addr': 0x0f,
    'resp': {
        (7, 6): None,
        (5, 0): None
    }
}

MDMCFG4 = {
    'desc': '',
    'addr': 0x10,
    'resp': {
        (7, 6): '',
        (5, 4): '',
        (3, 0): ''
    }
}

MDMCFG3 = {
    'desc': '',
    'addr': 0x11,
    'resp': {
        (7, 0): ''
    }
}

MDMCFG2 = {
    'desc': '',
    'addr': 0x12,
    'resp': {
        7: '',
        (6, 4): None,
        3: None,
        (2, 0): {
            0x00: 'no preamble/sync',
            0x01: '15/16',
            0x02: '16/16',
            0x03: '30/32',
            0x04: 'no preamble/sync, carrier sense',
            0x05: '',
            0x06: '',
            0x07: ''
        }
    }
}

MDMCFG1 = {
    'desc': '',
    'addr': 0x13,
    'resp': {
        7: '',
        (6, 4): {
            0x00: '2',
            0x01: '3',
            0x02: '4',
            0x03: '6',
            0x04: '8',
            0x05: '12',
            0x06: '16',
            0x07: '24',
        },
        (3, 2): None,
        (1, 0): ''
    }
}

MDMCFG0 = {
    'desc': '',
    'addr': 0x14,
    'resp': {
        (7, 0): ''
    }
}

DEVIATN = {
    'desc': '',
    'addr': 0x15,
    'resp': {
        7: None,
        (6, 4): {

        },
        3: None,
        (2, 0): {

        } 
    }
}

MCSM2 = {
    'desc': '',
    'addr': 0x16,
    'resp': {
        (7, 5): None,
        4: '',
        3: '',
        (2, 0): {

        }
    }
}

MCSM1 = {
    'desc': '',
    'addr': 0x17,
    'resp': {
        (7, 6): None,
        (5, 4): {
            0x00: 'always',
            0x01: 'below threshold',
            0x02: 'receiving packet',
            0x03: 'below threshold unless receiving'
        },
        (3, 2): {
            0x00: 'idle',
            0x01: 'FSTXON',
            0x02: 'TX',
            0x03: 'stay in RX'
        },
        (1, 0): {
            0x00: 'idle',
            0x01: 'FSTXON',
            0x02: 'stay in TX',
            0x03: 'RX'
        }
    }
}

MCSM0 = {
    'desc': '',
    'addr': 0x18,
    'resp': {
        (7, 6): None,
        (5, 4): {
            0x00: '',
            0x01: '',
            0x02: '',
            0x03: ''
        },
        (3, 2): {
            0x00: '1',
            0x01: '16',
            0x02: '64',
            0x03: '256'
        },
        1: 'radio control option',
        0: 'XOSC sleep state'
    }
}

FOCCFG = {
    'desc': 'frequency offset',
    'addr': 0x19,
    'resp': {
        (7, 6): '',
        5: '',
        (4, 3): '',
        2: '',
        (1, 0): ''
    }
}

BSCFG = {
    'desc': 'bit synch',
    'addr': 0x1a,
    'resp': {
        (7, 6): {
            0x00: 'k',
            0x01: '2k',
            0x02: '3k',
            0x03: '4k',
        },
        (5, 4): {
            0x00: 'k',
            0x01: '2k',
            0x02: '3k',
            0x03: '4k',
        },
        3: '',
        2: '',
        (1, 0): {
            0x00: '0',
            0x01: '3.125 %',
            0x02: '6.25 %',
            0x03: '12.5 %',
        },
    }
}

PARTNUM = {
    'addr': 0x30,
    'resp': {
        (7, 0): 'part number'
    }
}

VERSION = {
    'addr': 0x31,
    'resp': {
        (7, 0): 'chip version'
    }
}

MARCSTATE = {
    'addr': 0x35,
    'resp': {
        (7, 5): None,
        (4, 0): {

        }
    }
}